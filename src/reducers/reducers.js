import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import UserReducer from "./UserReducer";
import HistoryReservationReducer from "./HistoryReservationReducer";


const reducers = combineReducers({
    routing: routerReducer,
    user: UserReducer,
    historyReservation: HistoryReservationReducer
});

export default reducers