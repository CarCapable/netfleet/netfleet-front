import * as types from "../actions/actionsTypes";

const initialState = {
    isAuthenticated: false,
    email: null,
    id: null,
    roles: [],
    users: [],
    token: null,
    username: null,
    admin: false,
    error: false,
    tokenTimeOut: null
};

const UserReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CURRENT_USER_ADD:
            return {
                ...state,
                isAuthenticated: true,
                email: action.data.currentUser.email,
                id: action.data.currentUser.id,
                roles: action.data.currentUser.roles,
                token: action.data.currentUser.token,
                username: action.data.currentUser.username,
                error: false,
                tokenTimeOut: action.data.tokenTimeOut
            };
        case types.CURRENT_USER_REMOVE:
            return {
                ...state,
                isAuthenticated: false,
                email: null,
                id: null,
                roles: [],
                token: null,
                username: null,
                admin: false,
                tokenTimeOut: null
            };
        case types.USERS_GET:
            return {
                ...state,
                users: action.data.users
            };
        case types.USER_GET:
            return {
                ...state,
                user: action.data.user
            };
        case types.AUTHENTIFICATION_ERROR_ADD:
            return {
                ...state,
                error: true
            };
        case types.AUTHENTIFICATION_ERROR_REMOVE:
            return {
                ...state,
                error: false
            };
        default:
            return state;
    }
};

export default UserReducer;