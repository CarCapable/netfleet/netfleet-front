import * as types from "../actions/actionsTypes";

const initialState = {
    id: null,
    email: null,
    applicant: null,
    vehicle: null,
    startDate: null,
    endDate: null,
    departurePlace: null,
    arrivalPlace: null,
    status: null,
    refusalReason: null,
    carpoolPossible: false,
    description: null,
    carpoolRequests: [],
    carpoolUser: []
};

const HistoryReservationReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.RESERVATIONS_HISTORY_USER:
            return {
                ...state,
                applicant: action.data.user
            };
        default:
            return state;
    }
};

export default HistoryReservationReducer;