import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Header from "./components/header/Header";
import AuthenContainer from "./containers/AuthenContainer";


class App extends React.Component {

    render() {
        return (
            <div className="App">
                <Header/>
                <AuthenContainer/>
                {this.props.children}
            </div>
        );
    }
}

export default App;
