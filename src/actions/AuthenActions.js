import * as types from "./actionsTypes";
import {getBackUrl, TOKEN_TIME_OUT} from "../config/env";
import {addCurrentUser} from "./UserActions";

export function connexion(email, password) {
    return function (dispatch, getState) {
        fetch(getBackUrl() + "api/auth/signin", {
            headers: {
                // 'authorization': 'Bearer ' + getState().user.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({username: email, password: password})
        })
            .then(data => {
                if (!data.ok) {
                    throw data;
                }
                return data.json();
            })
            .then(data => {
                let tokenTimeOut = 0;
                tokenTimeOut = Date.now() + TOKEN_TIME_OUT;
                dispatch(addCurrentUser(data, tokenTimeOut));
            })
            .catch(data => {
                dispatch(anthentificationError());
            })
    }
}

export function anthentificationError() {
    return {
        type: types.AUTHENTIFICATION_ERROR_ADD
    };
}

export function anthentificationInitialError() {
    return {
        type: types.AUTHENTIFICATION_ERROR_REMOVE
    };
}