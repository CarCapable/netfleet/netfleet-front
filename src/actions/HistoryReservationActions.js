import * as types from "./actionsTypes";
import {getBackUrl} from "../config/env";

export function getHistoryReservationsByApplicantId(idApplicant) {
    return function (dispatch, getState) {
        fetch(getBackUrl() + "rest/reservationRequests/".concat(idApplicant), {
            headers: {
                'authorization': 'Bearer ' + getState().user.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(data => {
                if (!data.ok) {
                    throw data;
                }
                return data.json();
            })
            .catch(data => {
            dispatch(getHistoryReservationsByApplicantIdError());
        })
    }
}

export function getHistoryReservationsByApplicantIdError() {
    return {
        type: types.RESERVATIONS_HISTORY_USER_ERROR
    };
}
