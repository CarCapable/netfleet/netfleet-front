import * as types from "./actionsTypes";
import {getBackUrl} from "../config/env";
import {useStore} from "react-redux";
import {USERS_GET} from "./actionsTypes";

export function addCurrentUser(currentUser) {
    return {
        type: types.CURRENT_USER_ADD,
        data: {
            currentUser: currentUser
        }
    }
}

export function removeCurrentUser() {
    return {
        type: types.CURRENT_USER_REMOVE
    }
}

export function addAllUsers(users) {
    return {
        type: types.USERS_GET,
        data: {
            users: users
        }
    }
}

export function addUser(user, tokenTimeOut) {
    return {
        type: types.USER_GET,
        data: {
            user: user,
            tokenTimeOut: tokenTimeOut
        }
    }
}

export function getAllUsers() {
    return function (dispatch, getState) {
        fetch(getBackUrl() + "rest/users", {
            headers: {
                'authorization': 'Bearer ' + getState().user.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(data => {
                if (!data.ok) {
                    throw data;
                }
                return data.json();
            })
            .then(data => {
                dispatch(addAllUsers(data));
            }).catch(data => {
                dispatch(getAllUsersError());
        })
    }
}

export function getUsersByRole(role) {
    return function (dispatch, getState) {
        fetch(getBackUrl() + "rest/users/".concat(role), {
            headers: {
                'authorization': 'Bearer ' + getState().user.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(data => {
                if (!data.ok) {
                    throw data;
                }
                return data.json();
            })
            .then(data => {
                dispatch(addAllUsers(data));
            }).catch(data => {
                dispatch(getAllUsersError());
        })
    }
}

export function getUserById(idUser) {
    return function (dispatch, getState) {
        fetch(getBackUrl() + "rest/user/".concat(idUser), {
            headers: {
                'authorization': 'Bearer ' + getState().user.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(data => {
                if (!data.ok) {
                    throw data;
                }
                return data.json();
            })
            .then(data => {
                dispatch(addUser(data));
            }).catch(data => {
                dispatch(getAllUsersError());
        })
    }
}

export function getAllUsersError() {
    return {
        type: types.USERS_GET_ERROR
    };
}
