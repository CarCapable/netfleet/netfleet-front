const BACK_URL = "http://localhost";
const BACK_PORT = "8080";
export const TOKEN_TIME_OUT = 86100000;

export function getBackUrl(){
    return BACK_URL.trim() + (BACK_PORT.trim() !== "" ? ":" + BACK_PORT : "") + "/";
}

export function getUrlAnomyme(){
    return ["/login", "/password-forgot"];
}