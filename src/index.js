import React                            from 'react';
import ReactDOM                         from 'react-dom';
import './index.css';
import thunkMiddleware                  from 'redux-thunk'
import * as serviceWorker               from './serviceWorker';
import { Provider }                     from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { persistReducer, persistStore } from 'redux-persist'
import storage                          from 'redux-persist/lib/storage'
import { PersistGate }                  from 'redux-persist/integration/react'
import reducers                         from './reducers/reducers'
import { Route, Switch }                from 'react-router'
import { BrowserRouter }                from 'react-router-dom';
import PageNotFound                     from './components/errors/PageNotFound';
import LoginContainer                   from './containers/LoginContainer';
import App                              from './App';
import DetailsVehicule                  from './components/ManagementVehicle/DetailsVehicule';
import ForgottenPasswordContainer       from './containers/ForgottenPasswordContainer';
import HomeContainer                    from './containers/HomeContainer';
import VehiculeManagementContainer      from './containers/VehiculeManagementContainer';
import ReservationHistoryContainer      from './containers/ReservationHistoryContainer';
import DetailReservationHistory         from './components/ReservationHistory/DetailReservationHistory'
import AddNewVehicule                   from './components/ManagementVehicle/AddNewVehicule';
import ModifyVehicle                    from './components/ManagementVehicle/management-vehicule-component/ModifyVehicle';
import UserListContainer                from './containers/UserListContainer';
import UserDetail                       from './components/user/UserDetail';
import ModifyUser                       from "./components/user/user-components/ModifyUser";
import AddNewUser                       from "./components/user/user-components/AddNewUser";
import ReservationVehicleContainer      from "./containers/ReservationVehicleContainer";
import DetailsVehicleReservation        from "./components/reservation/DetailsVehicleReservation";
import AddNewReservation                from './components/reservation/AddNewReservation';

const persistConfig = {
    key: 'root',
    storage
};
const persistedReducer = persistReducer(persistConfig, reducers);
let store = createStore(persistedReducer,
    applyMiddleware(
        thunkMiddleware
    )
);
let persistor = persistStore(store);

//si vous voulez "purger" ce que vous avez enregistré
//persistStore(store).purge();

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
                <App>
                    <Switch>
                        <Route exact path="/login" component={LoginContainer}/>
                        <Route exact path="/" component={HomeContainer}/>
                        <Route exact path="/home" component={HomeContainer} />
                        <Route exact path="/reservation-history" component={ReservationHistoryContainer} />
                        <Route exact path="/forgotten-password" component={ForgottenPasswordContainer}/>
                        <Route exact path="/management-vehicle" component={VehiculeManagementContainer}/>
                        <Route exact path="/user-list" component={UserListContainer} />
                        <Route exact path="/user-detail/:idUser" component={UserDetail}/>
                        <Route exact path="/user-detail/:idUser" component={UserDetail}/>
                        <Route exact path="/updateUser/:idUser" component={ModifyUser}/>
                        <Route exact path="/addNewUser/" component={AddNewUser}/>
                        <Route exact path="/reservation-vehicle" component={ReservationVehicleContainer}/>
                        <Route exact path="/reservation-vehicle/details-vehicule/ajouter-reservation/:idVehicule" component={AddNewReservation}/>
                        <Route exact path="/reservation-vehicle/details-vehicule/:idVehicule" component={DetailsVehicleReservation}/>
                        <Route exact path="/management-vehicle/modifier-vehicle/:idVehicule" component={ModifyVehicle}/>
                        <Route exact path="/management-vehicle/details-vehicule/:idVehicule" component={DetailsVehicule}/>
                        <Route exact path="/management-vehicle/ajouter-vehicule" component={AddNewVehicule}/>
                        <Route path="*" component={PageNotFound}/>
                    </Switch>
                </App>
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
