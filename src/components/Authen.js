import React from 'react';
import {Redirect} from "react-router";
import {getUrlAnomyme} from "../config/env";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.boucle = null;
        this.state = {
            timeOut: null
        }
        this.checkToken = this.checkToken.bind(this);
    }


    componentDidMount() {
        const self = this;
        // console.log(self.props.store.user);
        this.boucle = setInterval(function(){ self.checkToken(); }, 15000);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const self = this;
        clearInterval(this.boucle);
        if(this.props.store.user.tokenTimeOut !== prevProps.store.user.tokenTimeOut && this.props.store.user.tokenTimeOut === null){
            self.setState({timeOut: null});
        }
        this.boucle = setInterval(function(){ self.checkToken(); }, 15000);
    }

    checkToken(){

        if (this.props.store.user.isAuthenticated) {
            let timeOut = null;
            if (this.props.store.user.tokenTimeOut !== null) {
                if((this.props.store.user.tokenTimeOut + 60000) <= Date.now()){
                    this.props.logout();
                }
                if(this.props.store.user.tokenTimeOut - Date.now() <= 1800000){
                    if(((this.props.store.user.tokenTimeOut - Date.now()) / 60000).toFixed() > 0){
                        timeOut = ((this.props.store.user.tokenTimeOut - Date.now()) / 60000).toFixed();
                    }
                    else{
                        timeOut = 1;
                    }
                    this.setState({timeOut: timeOut});
                }
            }
        }
        else{
            if(this.props.store.user.tokenTimeOut === null && this.state.timeOut !== null)
                this.setState({timeOut: null});
        }
    }

    render() {
        const {user} = this.props.store;
        if (!user.isAuthenticated && !getUrlAnomyme().includes(window.location.pathname)) {
            return <Redirect to='/login'/>;
        }
        if (user.isAuthenticated && getUrlAnomyme().includes(window.location.pathname)) {
            return <Redirect to='/home'/>;
        }
        if(this.state.timeOut === null){
            return("");
        }
        const style = {
            lineHeight: "20px",
            backgroundColor: "#F58C02",
            textAlign: "center",
            height: "20px"
        }
        return (
            <div style={style}>
                Votre session va expirer dans {this.state.timeOut} minute(s)
            </div>
        )
    }
}

export default App;
