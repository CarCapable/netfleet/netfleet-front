import * as React from "react";

class AccessDenied extends React.Component{

    render() {
        return (
            <div className="box page-error">
                <div className="title">
                    Erreur 403
                </div>
                <div>
                    Vous n'avez pas accès à cette page.
                </div>
            </div>
        )
    }
}

export default AccessDenied;