import * as React from "react";

class PageNotFound extends React.Component{

    render() {
        return (
            <div className="box page-error">
                <div className="title">
                    Erreur 404
                </div>
                <div>
                    La page demandée n'existe pas.
                </div>
            </div>
        )
    }
}

export default PageNotFound;