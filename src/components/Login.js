import * as React from "react";
import {Redirect} from "react-router";

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            errorEmail: false,
            errorPassword: false,
            isPasswordForgotten: false,
            error: props.store.error
        };

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onClickLogin = this.onClickLogin.bind(this);
        this.onClickForgottenPassword = this.onClickForgottenPassword.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (this.props !== prevProps) {
            this.setState({error: this.props.store.error})
        }
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value,
            errorEmail: false,
            error: false
        })
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value,
            errorPassword: false,
            error: false
        })
    }

    onClickLogin() {
        const {email, password} = this.state;
        let errorEmail = email.trim() === "";
        let errorPassword = password.trim() === "";
        this.setState({
            errorEmail: errorEmail,
            errorPassword: errorPassword,
            error: false
        });
        if (!errorEmail && !errorPassword) {

            this.props.connexion(email, password);
        }
    }

    onClickForgottenPassword(){

        this.props.initialError();
        this.setState({
            isPasswordForgotten: true
        })
    }

    render() {

        const {errorEmail, errorPassword, isPasswordForgotten, error} = this.state;

        if (isPasswordForgotten) {
            return <Redirect to='/forgotten-password'/>;
        }
        return (
            <div id="login">
                <div className="middle-box">
                    <div className="escape text-error">
                        {error &&
                        <>Erreur : Le mail ou le mot de passe est invalide !</>
                        }
                    </div>
                    <div>
                        <input onChange={this.onChangeEmail} type="text" placeholder="Login" value={this.state.email}/>
                    </div>
                    <div className="escape text-error">
                        {errorEmail &&
                        <>Erreur : Il n'y a pas d'adresse mail !</>
                        }
                    </div>
                    <div>
                        <input onChange={this.onChangePassword} type="password" placeholder="Mot de passe"
                               value={this.state.password}/>
                    </div>
                    <div className="escape text-error">
                        {errorPassword &&
                        <>Erreur : Il n'y a pas de mot de passe !</>
                        }
                    </div>
                    <hr/>
                    <button onClick={this.onClickForgottenPassword}>Mot de passe oublié ?</button>
                    <button onClick={this.onClickLogin}>Connexion</button>
                </div>
            </div>
        )
    }
}

export default Login;