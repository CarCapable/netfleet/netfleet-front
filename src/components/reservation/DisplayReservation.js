import React from "react";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {Redirect} from "react-router";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import {Typography} from '@material-ui/core';
import {getBackUrl} from "../../config/env";
import Divider from "@material-ui/core/Divider";

export default class DisplayReservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: JSON.parse(localStorage.getItem('user')),
            isRedirectToDetailsVehicle: false,
            carpoolPossible: this.props.reservation.carpoolPossible && this.props.reservation.users.length + 1 < this.props.reservation.nbPlace,
            alreadyCarpool: this.props.reservation.users.filter((userSearch) =>
                userSearch.id == JSON.parse(localStorage.getItem('user')).id
            ).length !== 0 || this.props.reservation.idCreatorReservation === JSON.parse(localStorage.getItem('user')).id
        };
        this.onClickDetailsVehicule = this.onClickDetailsVehicule.bind(this)
        this.onClickaddCarPool = this.onClickaddCarPool.bind(this)
    }

    onClickDetailsVehicule() {
        this.setState({
            isRedirectToDetailsVehicle: true
        })
    }

    onClickaddCarPool() {
        fetch(getBackUrl() + "rest/reservationRequests/addCarpool", {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + this.state.user.token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idUser: this.state.user.id,
                idReservation: this.props.reservation.id
            })
        })
        window.location.reload(false)
    }


    render() {

        const {isRedirectToDetailsVehicle, carpoolPossible, alreadyCarpool} = this.state;

        if (isRedirectToDetailsVehicle) {
            return <Redirect to={"/reservation-vehicle/details-vehicule/" + this.props.vehicule.id}/>;
        }


        const paper={
            height:'80%',
            width:'80%',
            margin: 5
        }

        return (
            <Paper elevation={2} style={paper}>
                <Grid container direction={"row"} spacing={1}>

                    <Grid container item sm={8} justify={"flex-start"} alignItems={"center"} >
                        <Grid item sm={12} xs={12}>
                            <Typography variant="h6" color={"inherit"} noWrap={true}>
                                Date de début : {this.props.reservation.startDate}
                            </Typography>
                        </Grid>


                        <Grid item sm={12} xs={12}>
                            <Typography variant="h6" color={"inherit"} noWrap={true}>
                                Date de fin : {this.props.reservation.endDate}
                            </Typography>
                        </Grid>


                        <Grid item sm={12} xs={12}>
                            <Typography variant="h6" color={"inherit"} noWrap={true}>
                                Réservé par : {this.props.reservation.nameApplicant}
                            </Typography>
                        </Grid>

                        <Grid item sm={12} xs={12}>
                            <Typography variant="h6" color={"inherit"} noWrap={true}>
                                Description: {this.props.reservation.description}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid container item sm={4} justify={"center"} alignItems={"center"}>
                        <Box component="span" visibility={carpoolPossible ? "visible" : "hidden"}>
                            <Grid style={{textAlign: "center"}}>
                                <Button variant="contained" color="default" disabled={alreadyCarpool}
                                        onClick={this.onClickaddCarPool}>
                                    Ajout covoiturage
                                </Button>
                            </Grid>
                        </Box>
                    </Grid>

                </Grid>
            </Paper>
        )
    }

}