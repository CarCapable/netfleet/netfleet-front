import React from "react";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import {Redirect} from "react-router";
import {Box} from "@material-ui/core";

export default class DisplayVehiculeReservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isRedirectToDetailsVehicle: false
        };
        this.onClickDetailsVehicule = this.onClickDetailsVehicule.bind(this)
    }

    onClickDetailsVehicule() {
        this.setState({
            isRedirectToDetailsVehicle: true
        })
    }


    render() {

        const {isRedirectToDetailsVehicle} = this.state;

        if (isRedirectToDetailsVehicle) {
            return <Redirect to={"/reservation-vehicle/details-vehicule/" + this.props.vehicule.id}/>;
        }
        return (
            <Paper elevation={3}>
                <Box m={5}/>
                <Grid container direction={"row"} alignItems={"center"} justify={"space-evenly"} spacing={1}>
                    <Grid item sm={6}>
                        <img src={this.props.vehicule.image} width={250}/>
                    </Grid>
                    <Grid item sm={4}>
                        <p className="h1">{this.props.vehicule.brand}</p>
                        <p className="h3">{this.props.vehicule.fuel.value}</p>
                        <p className="h5">{this.props.vehicule.nbSeats} places</p>
                    </Grid>
                    <Grid item sm={2}>
                        <a onClick={this.onClickDetailsVehicule}><ArrowForwardIcon></ArrowForwardIcon></a>
                    </Grid>
                </Grid>
            </Paper>
        )
    }

}