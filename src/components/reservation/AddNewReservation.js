import React from "react";
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
import {Redirect} from "react-router";
import Box from "@material-ui/core/Box";
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import {DateRangePicker} from 'react-date-range';
import {getBackUrl} from "../../config/env";

export default class AddNewReservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.idVehicule,
            user: JSON.parse(localStorage.getItem('user')),
            addable: true,
            reservation: [],
            rangeDatesDisabled: [],
            startDate: new Date(),
            endDate: new Date(),
            description: "",
            covoiturage: "",
            isRedirectToReservation: false
        };
        this.handleValueCovoiturage = this.handleValueCovoiturage.bind(this)
        this.redirectToReservation = this.redirectToReservation.bind(this)
        this.handleValueDescription = this.handleValueDescription.bind(this)
        this.addNewReservation = this.addNewReservation.bind(this)
    }

    redirectToReservation() {
        this.setState({
            isRedirectToReservation: true
        })
    }

    componentDidMount() {
        fetch(getBackUrl() + "rest/reservationRequests/vehicle/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({
                reservation: data, loading: true
            }))
    }

    addNewReservation() {
        fetch(getBackUrl() + "rest/reservationRequests/addReservationRequest", {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + this.state.user.token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                description: this.state.description,
                endDate: this.state.endDate,
                startDate: this.state.startDate,
                idVehicle: this.state.id,
                idApplicant: this.state.user.id,
                carpoolPossible: this.state.covoiturage == "true"
            })
        })
        this.redirectToReservation();
    }

    onChangeStartDate(date) {
        this.setState({
            startDate: date.selection.startDate,
            endDate: date.selection.endDate
        })
    }

    isAddable() {
        const {
            description,
            covoiturage,
        } = this.state;
        if (description !== ""
            && covoiturage !== "") {
            this.setState({addable: false})
        } else {
            this.setState({addable: true})
        }
    }

    handleValueDescription(value) {
        this.setState({description: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueCovoiturage(value) {
        this.setState({covoiturage: value.target.value}, () =>
            this.isAddable()
        )
    }

    render() {
        const {isRedirectToReservation, reservation} = this.state;

        let rangeDatesDisabled = this.state.rangeDatesDisabled;

        if (isRedirectToReservation) {
            return <Redirect to={"/reservation-vehicle"}/>;
        }

        let selectionRange = {
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            key: 'selection'
        }

        const getDatesBetweenDates = (startDate, endDate) => {
            let dates = []
            const theDate = new Date(startDate)
            while (theDate < endDate) {
                dates = [...dates, new Date(theDate)]
                theDate.setDate(theDate.getDate() + 1)
            }
            dates = [...dates, endDate]
            return dates
        }

        for (let value in reservation) {
            var table = getDatesBetweenDates(new Date(reservation[value].startDate), new Date(reservation[value].endDate))
            rangeDatesDisabled = rangeDatesDisabled.concat(table)
        }

        return (
            <Grid container justify={"center"}>
                <Grid item sm={8}>
                    <Card>
                        <CardContent>
                            <Typography variant="h4" gutterBottom>
                                Ajout d'une réservation
                            </Typography>
                            <Grid container justify={"center"} spacing={3}>
                                <Grid item xs={12} sm={12}>
                                    <DateRangePicker
                                        ranges={[selectionRange]}
                                        onChange={(date) => this.onChangeStartDate(date)}
                                        disabledDates={rangeDatesDisabled}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <TextField
                                        required
                                        id="description"
                                        name="description"
                                        label="description"
                                        fullWidth
                                        onChange={this.handleValueDescription}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <FormLabel component="legend" required={true}>Covoiturage</FormLabel>
                                    <RadioGroup aria-label="type" name="type"
                                                onChange={this.handleValueCovoiturage}>
                                        <Grid container>
                                            <Grid>
                                                <FormControlLabel value="true" control={<Radio/>} label="Possible"/>
                                            </Grid>
                                            <Grid>
                                                <FormControlLabel value="false" control={<Radio/>}
                                                                  label="Impossible"/>
                                            </Grid>
                                        </Grid>
                                    </RadioGroup>
                                </Grid>

                                <Box m={2}/>
                                <Grid container justify={"center"}>
                                    <Grid item sm={2}>
                                        <Button variant="contained" color="primary" onClick={this.addNewReservation}
                                                disabled={this.state.addable}>
                                            Ajouter
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }

}
