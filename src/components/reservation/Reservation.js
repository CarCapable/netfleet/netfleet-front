import React                      from "react";
import Grid                       from "@material-ui/core/Grid";
import CircularProgress           from "@material-ui/core/CircularProgress/CircularProgress";
import FilterSearchVehicule       from "../ManagementVehicle/management-vehicule-component/FilterSearchVehicule";
import DisplayVehiculeReservation from "./DisplayVehicleReservation";
import {getBackUrl} from "../../config/env";

class Reservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: props.store.user,
            vehicules: [],
            vehiculesState: []
        };
        localStorage.setItem("user", JSON.stringify(this.state.user))
    }

    componentDidMount() {
        fetch(getBackUrl() + "rest/vehicles/available",
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({ vehicules: data, vehiculesState: data, loading: true }))
    }

    onChangeTypeOfVehicule = (typeOfVehicule) => {
        if (typeOfVehicule !== this.state.typeOfVehicule) {
            this.setState({ typeOfVehicule: typeOfVehicule }, () => {
                this.filterVehicule()
            })
        }
    }

    onChangeNbPlace = (nbPlace) => {
        if (nbPlace !== this.state.nbPlace) {
            this.setState({ nbPlace: nbPlace }, () => {
                this.filterVehicule()
            })
        }
    }

    filterVehicule() {
        this.setState({ vehicules: this.state.vehiculesState });

        let vehiculesFiltered = this.state.vehiculesState;

        if (this.state.nbPlace !== 0 && this.state.nbPlace !== "Selectionner le nombre de place") {
            vehiculesFiltered = vehiculesFiltered.filter(vehicule => vehicule.nbSeats === parseInt(this.state.nbPlace));
        }

        if (this.state.typeOfVehicule !== "" && this.state.typeOfVehicule !== "Selectionner le type") {
            vehiculesFiltered = vehiculesFiltered.filter(vehicule => vehicule.fuel.value === this.state.typeOfVehicule);
        }

        this.setState({ vehicules: vehiculesFiltered })
    }

    render() {

        if (!this.state.loading) {
            return <Grid container justify={"center"}>
                <CircularProgress color="secondary"/>
            </Grid>
        }

        return (
            <Grid container justify="center" alignItems={"center"} spacing={4}>
                <Grid item sm={8}>
                    <FilterSearchVehicule typeOfVehiculeChange={this.onChangeTypeOfVehicule}
                                          nbPlaceChange={this.onChangeNbPlace}/>
                </Grid>
                <Grid container spacing={3} alignItems={"center"} justify="center">
                    <Grid item sm={8}>
                        {this.state.vehicules.map((vehicule, index) => {
                            return <DisplayVehiculeReservation key={index} vehicule={vehicule}/>
                        })}
                    </Grid>
                </Grid>
            </Grid>
        )

    }
}

export default Reservation;
