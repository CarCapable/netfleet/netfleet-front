import React from "react";
import Grid from '@material-ui/core/Grid';
import Paper from "@material-ui/core/Paper/Paper";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import DisplayReservation from "./DisplayReservation";
import Button from '@material-ui/core/Button';
import {Redirect} from 'react-router';
import {getBackUrl} from "../../config/env";
import Divider from "@material-ui/core/Divider";

export default class DetailsVehicleReservation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.idVehicule,
            user: JSON.parse(localStorage.getItem('user')),
            vehicule: {},
            vehicleActive: false,
            reservation: [],
            loading: false,
            isRedirectToAddReservation: false
        };
        this.onClickAddReservationVehicle = this.onClickAddReservationVehicle.bind(this)
    }

    componentDidMount() {
        fetch(getBackUrl() + "rest/vehicles/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({
                vehicule: {
                    id: data.id,
                    brand: data.brand,
                    fuel: data.fuel.value,
                    model: data.model,
                    nbSeats: data.nbSeats,
                    trunkCapacity: data.trunkCapacity,
                    stickGear: data.stickGear ? "MANUEL" : "AUTOMATIQUE",
                    image: data.image,
                    keysPosition: data.keysPosition,
                    vinCode: data.vinCode
                }
            }));

        fetch(getBackUrl() + "rest/reservationRequests/vehicle/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({
                reservation: data, loading: true
            }))
    }

    changeAddedAppointment(addedAppointment) {
        this.setState({addedAppointment});
    }

    onClickAddReservationVehicle() {
        this.setState({
            isRedirectToAddReservation: true
        })
    }

    render() {

        const {vehicule, isRedirectToAddReservation} = this.state;

        if (!this.state.loading) {
            return <Grid container justify={"center"}>
                <CircularProgress color="secondary"/>
            </Grid>
        }

        if (isRedirectToAddReservation) {
            return <Redirect to={"/reservation-vehicle/details-vehicule/ajouter-reservation/" + this.state.id}/>;
        }

        return (
            <Grid container alignItems={"center"} direction={"column"} spacing={3}>
                <Box m={2}/>

                <Grid item sm={12}>
                    <Paper elevation={3}>
                        <img src={vehicule.image} width={750}/>
                    </Paper>
                </Grid>

                <Box m={2}/>

                {/* Container of 2 row */}
                <Grid container item sm={11} spacing={2}>

                    {/* FIRST ROW */}
                    <Grid item sm={4} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Modèle du véhicule</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.model}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Marque</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.brand}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Nombre de place</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.nbSeats}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    {/* SECOND ROW */}
                    <Grid item sm={4} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Capacité du coffre</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.trunkCapacity} L</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Carburant</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.fuel}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Boite de vitesse</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.stickGear}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    <Grid item sm={6} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Position des clés</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.keysPosition}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    <Grid item sm={6} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Immatriculation</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.vinCode}</p>
                            </Grid>
                        </Paper>
                    </Grid>


                </Grid>


                <Grid container item sm={12} justify={"center"}>
                    <Button variant="contained" color="primary" onClick={this.onClickAddReservationVehicle}>Ajouter une
                        réservation</Button>
                </Grid>


                <Grid container item sm={12} direction={"row"} justify={"center"} spacing={3}>
                        {this.state.reservation.map((reservation, index) => {
                            return <DisplayReservation key={index} reservation={reservation}/>
                        })}
                </Grid>
            </Grid>

        )
    }
}
