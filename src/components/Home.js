import * as React from "react";
import {Redirect} from "react-router";

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLogout: false,
            isRedirectToUsers: false,
            isRedirectToManagementVehicle: false,
            isRedirectToReservationVehicle: false,
            isRedirectToHistoryReservation: false
        };
        this.onClickLogout = this.onClickLogout.bind(this);
        this.onClickUserList = this.onClickUserList.bind(this);
        this.onClickManagementVehicle = this.onClickManagementVehicle.bind(this);
        this.onClickReservationHistory = this.onClickReservationHistory.bind(this);
        this.onClickDemandeReservationVehicle = this.onClickDemandeReservationVehicle.bind(this);
    }

    onClickLogout() {

        this.props.logout();
    }


    onClickUserList() {
        this.setState({
            isRedirectToUsers: true
        })
    }

    onClickManagementVehicle() {
        this.setState({
            isRedirectToManagementVehicle: true
        })
    }

    onClickReservationHistory() {
        this.setState({
            isRedirectToHistoryReservation: true
        })
    }

    onClickDemandeReservationVehicle() {
        this.setState({
            isRedirectToReservationVehicle: true
        })
    }


    render() {

        const {user} = this.props.store;
        const {isRedirectToUsers} = this.state;
        const {isRedirectToManagementVehicle} = this.state;
        const {isRedirectToHistoryReservation} = this.state;
        const {isRedirectToReservationVehicle} = this.state;

        if (isRedirectToUsers) {
            return <Redirect to='/user-list'/>;
        }

        if (isRedirectToManagementVehicle) {
            return <Redirect to='/management-vehicle'/>;
        }

        if (isRedirectToHistoryReservation) {
            return <Redirect to='/reservation-history'/>
        }


        if (isRedirectToReservationVehicle) {
            return <Redirect to='/reservation-vehicle'/>;
        }

        return (
            <div id="accueil" className="middle-box">
                <button onClick={this.onClickDemandeReservationVehicle}>Demander une réservation</button>
                <button onClick={this.onClickReservationHistory}>Mes réservations</button>
                {user.roles.includes('ROLE_ADMIN') &&
                <>
                    <button onClick={this.onClickUserList}>Gestion utilisateurs</button>
                    <button onClick={this.onClickManagementVehicle}>Gestion véhicules</button>
                </>
                }
                <button onClick={this.onClickLogout}>Déconnexion</button>
            </div>
        )
    }
}

export default Home;