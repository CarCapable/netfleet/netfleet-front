import React                            from 'react';
import FilterSearchHistoryReservation   from './reservation-history-component/FilterSearchHistoryReservation';
import Grid                             from '@material-ui/core/Grid';
import DisplayReservationHistory        from './reservation-history-component/DisplayReservationHistory';
import CircularProgress                 from '@material-ui/core/CircularProgress';
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from "@material-ui/core/TableBody";
import {getBackUrl} from "../../config/env";

export default class ReservationHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: props.store.user,
            loading: false,
            dateOfReservationHistory: null,
            reservationsHistory: [],
            reservationsHistoryList: []
        }
        localStorage.setItem("user", JSON.stringify(this.state.user))
    };

    onChangeDate = (dateOfReservationHistory) => {
        if (dateOfReservationHistory !== this.state.dateOfReservationHistory) {
            this.setState({dateOfReservationHistory: dateOfReservationHistory}, () => {
                this.filterReservationHistory()
            })
        }
    }

    filterReservationHistory() {
        let reservationsHistoryFiltered = this.state.reservationsHistory;

        if (this.state.dateOfReservationHistory !== null) {
            reservationsHistoryFiltered = reservationsHistoryFiltered.filter(reservationHistory => reservationHistory.startDate === this.state.dateOfReservationHistory);
        }

        this.setState({reservationsHistoryList:reservationsHistoryFiltered});
    }

    componentDidMount() {
        fetch(getBackUrl()+"rest/reservationRequests/"+this.state.user.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({reservationsHistory: data, reservationsHistoryList:data, loading: true}))
    }

    render() {


        if(!this.state.loading){
            return <Grid container justify={"center"}>
                <CircularProgress color="secondary" />
            </Grid>
        }

        return (
            <Grid container justify="center" alignItems={"center"} spacing={4}>
                <Grid item sm={8}>
                    <FilterSearchHistoryReservation dateHistoryReservation={this.onChangeDate}/>
                </Grid>
                <Grid container spacing={3} alignItems={"center"} justify="center">
                    <Grid item sm={8}>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>ID</TableCell>
                                        <TableCell>Date de départ</TableCell>
                                        <TableCell>Marque Voiture</TableCell>
                                        <TableCell>Modele Voiture</TableCell>
                                        <TableCell>Description</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.reservationsHistoryList.map((reservationHistory, key) => {
                                            return (
                                                <DisplayReservationHistory reservationHistory={reservationHistory} key={key}/>
                                            )
                                        })
                                    }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}
