import React from 'react';
import Grid  from '@material-ui/core/Grid';

export default class FilterSearchHistoryReservation extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const onChangeDate = (event) => {
            this.props.dateHistoryReservation(event.target.value);
        };

        return (
            <Grid container justify="center">
                <Grid item xs={12}>
                    <input className="browser-default" onChange={onChangeDate} type="date" placeholder="Selectionner la date de départ"/>
                </Grid>
            </Grid>
        )
    }

}
