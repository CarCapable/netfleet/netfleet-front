import React from 'react';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import DetailReservationHistory from "../DetailReservationHistory";

export default class DisplayReservationHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isRedirectToDetailsReservationHistory: false,
            reservationHistory: this.props.reservationHistory
        };
        this.onClickDetailsReservationHistory = this.onClickDetailsReservationHistory.bind(this)
    }

    onClickDetailsReservationHistory(e) {
        this.setState({
            isRedirectToDetailsReservationHistory: true
        })
    }


    render() {
        const {reservationHistory} = this.state;
        const {isRedirectToDetailsReservationHistory} = this.state;

        return (
            <>
                <TableRow onClick={this.onClickDetailsReservationHistory} key={reservationHistory.id} hover={true}>
                    <TableCell component="th" scope="row">
                        {reservationHistory.id}
                    </TableCell>
                    <TableCell>{reservationHistory.startDate}</TableCell>
                    <TableCell>{reservationHistory.vehicle.brand}</TableCell>
                    <TableCell>{reservationHistory.vehicle.model}</TableCell>
                    <TableCell>{reservationHistory.description}</TableCell>
                </TableRow>
            {
                isRedirectToDetailsReservationHistory && <DetailReservationHistory reservationHistory={reservationHistory}/>
            }</>
        )
    }

}