import React from "react";
import Grid from '@material-ui/core/Grid';
import {ListSubheader, Modal} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Box from "@material-ui/core/Box";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";

export default class DetailsReservationHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: JSON.parse(localStorage.getItem('user')),
            reservationHistory: this.props.reservationHistory,
            open: true
        };

    }

    onHandlerClose = () => {
        this.setState({open: false});
    }

    onHandlerOpen = () => {
        this.setState({open: true});
    }


    render() {

        const reservation = this.state.reservationHistory;
        const vehicule = reservation.vehicle;
        const listUserCarpool = reservation.carpoolUser.map(user => <ListItem
            key={user.id}><ListItemText>{user.identity.name} {user.identity.firstName}</ListItemText></ListItem>)


        const body = (
            <Card>
                <CardHeader title={"Reservation N°" + reservation.id}/>
                <CardContent>
                    <Grid container direction={"row"} alignItems={"center"} justify={"center"} spacing={2}>
                        <Grid item md={6} xs={12}>
                            Date Début : {reservation.startDate}
                        </Grid>
                        <Grid item md={6} xs={12}>
                            {reservation.carpoolPossible && 'Co-voiturage possible'}
                            {!reservation.carpoolPossible && 'Co-voiturage non possible'}
                        </Grid>
                        <Grid item  xs={12}>
                            Description : {reservation.description}
                        </Grid>
                    </Grid>
                    <Paper elevation={3}>
                        <Box m={5}/>
                        <Grid container direction={"row"} alignItems={"center"} justify={"space-evenly"} spacing={1}>
                            <Grid item sm={6}>
                                <img src={vehicule.image} width={250}/>
                            </Grid>
                            <Grid item sm={4}>
                                <p className="h1">{vehicule.brand} {vehicule.model}</p>
                                <p className="h3">{vehicule.value}</p>
                                <p className="h5">{vehicule.nbSeats} places</p>
                            </Grid>
                        </Grid>
                    </Paper>
                    <List dense>
                        <ListSubheader> Liste des utilisateurs participant au co-voiturage</ListSubheader>
                        {listUserCarpool}
                    </List>
                </CardContent>
            </Card>
        )
        return (
            <Modal open={this.state.open} onEscapeKeyDown={this.onHandlerClose} className="modal-custom">
                {body}
            </Modal>
        )
    }
}