import * as React from "react";
import {getBackUrl} from "../../config/env";
import Grid from "@material-ui/core/Grid";
import {Redirect} from "react-router";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper/Paper";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import Divider from "@material-ui/core/Divider";

class UserDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            idUser: props.match.params.idUser,
            currentUser: JSON.parse(localStorage.getItem('user')),
            isRedirectToModifyUser: false,
            user: {},
            roles: []
        };

        this.onClickDeleteRole = this.onClickDeleteRole.bind(this);
        this.onClickModifyUser = this.onClickModifyUser.bind(this);
    }

    componentDidMount() {
        console.log(this.state.currentUser);
        fetch(getBackUrl() + "rest/users/user/".concat(this.state.idUser), {
            headers: {
                'authorization': 'Bearer ' + this.state.currentUser.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    user: {
                        id: data.id,
                        name: data.identity.name,
                        firstName: data.identity.firstName,
                        telephone: data.identity.telephone,
                        email: data.identity.email,
                        username: data.username,
                        status: data.status ? "ACTIVÉ" : "DÉSACTIVÉ"
                    },
                    roles: data.roles
                });
            })
    }

    onClickDeleteRole(e) {
        console.log(e);
        const tempUser = this.state.user;
        tempUser.roles.splice(e);
        this.setState({
            user: tempUser
        })
    }

    onClickModifyUser() {
        this.setState({
            isRedirectToModifyUser: true
        })
    }

    render() {

        const {user, roles, isRedirectToModifyUser} = this.state;

        if (isRedirectToModifyUser) {
            return <Redirect to={"/updateUser/" + user.id}/>;
        }

        this.roles = roles.map((role, key) =>
            <TableRow key={role.id}>
                <TableCell><h4>{role.name === "ROLE_ADMIN" ? "Administrateur" : "Utilisateur"}</h4></TableCell>
            </TableRow>
        );

        return (
            <Grid container alignItems={"center"} direction={"column"} spacing={1}>

                <Grid item sm={12}>
                    <Box sm={5}/>
                    <Button  variant="contained" color="primary" onClick={this.onClickModifyUser}>Modifier l'utilisateur</Button>
                </Grid>

                <Box m={1}/>

                {/* Container of 2 row */}
                <Grid container sm={8} justify={"center"} direction={"row"} spacing={2}>
                    {/* FIRST ROW */}
                    <Grid item sm={6} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h4">Prénom</p>
                                <Box m={1}/>
                                <Divider variant="middle" />
                                <Box m={1}/>
                                <p className="h3">{user.firstName}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={6} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h4">Nom</p>
                                <Box m={1}/>
                                <Divider variant="middle" />
                                <Box m={1}/>
                                <p className="h3">{user.name}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    {/* SECOND ROW */}
                    <Grid item sm={6} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h4">Téléphone</p>
                                <Box m={1}/>
                                <Divider variant="middle" />
                                <Box m={1}/>
                                <p className="h3">{user.telephone}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={6} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h4">Email</p>
                                <Box m={1}/>
                                <Divider variant="middle" />
                                <Box m={1}/>
                                <p className="h3">{user.email}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    {/* THIRD ROW */}
                    <Grid item sm={6} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h4">Login</p>
                                <Box m={1}/>
                                <Divider variant="middle" />
                                <Box m={1}/>
                                <p className="h3">{user.username}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={6} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h4">Statut</p>
                                <Box m={1}/>
                                <Divider variant="middle" />
                                <Box m={1}/>
                                <p className="h3">{user.status}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={10}>
                        <TableContainer component={Paper} elevation={3}>
                            <Table className="table table-hover table-bordered">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align={"center"}><h4>Rôles</h4></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.roles}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default UserDetail;