import * as React from "react";
import UserDetail from "./UserDetail";
import {Redirect} from "react-router";
import UserTableLine from "./user-components/UserTableLine";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

class UserList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            users: props.store.users,
            currentUser: props.store.currentUser,
            isRedirectToNewUser: false
        };
        this.onChangeSelectedRole = this.onChangeSelectedRole.bind(this);
        this.onClickAddNewUser = this.onClickAddNewUser.bind(this);
    }

    componentDidMount() {
        this.props.getAllUsers();
    }

    onClickAddNewUser() {
        this.setState({
            isRedirectToAddNewUser: true
        })
    }

    onChangeSelectedRole(e) {
        if (e === undefined) {
            this.props.getAllUsers();
        } else {
            this.props.getUsersByRole(e.target.value);
        }
    }

    render() {

        let selectedRole = undefined;

        if (this.state.isRedirectToAddNewUser) {
            return <Redirect to={"/addNewUser"}/>;
        }

        return (
            <Grid container justify="center" alignItems={"center"} spacing={4}>
                <Grid item sm={8}>
                    <Button variant="contained" color="primary" onClick={this.onClickAddNewUser} fullWidth>Ajouter un utilisateur</Button>
                </Grid>
                <Grid item sm={8}>
                    <form>
                        <div className="form-group">
                            <label htmlFor="role-select">Filtrer par rôle</label>
                            <select className="form-control" id="role-select" value={selectedRole}
                                    onChange={this.onChangeSelectedRole}>
                                <option value={undefined} label="Tous"/>
                                <option value="ROLE_USER" label="Utilisateurs"/>
                                <option value="ROLE_ADMIN" label="Admin"/>
                            </select>
                        </div>
                    </form>
                </Grid>
                <Grid container spacing={3} alignItems={"center"} justify="center">
                    <Grid item sm={8}>
                        <table className="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">Prénom</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Login</th>
                                <th scope="col">Téléphone</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.props.store.users.map((user, key) => {
                                    return (
                                    <UserTableLine user={user} key={key} currentUser={this.props.store.currentUser}/>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}


export default UserList;