import * as React from "react";
import {Redirect} from "react-router";

class UserTableLine extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isRedirectToDetail: false,
            user: this.props.user,
            currentUser: this.props.currentUser
        };
        this.onClickDetail = this.onClickDetail.bind(this);
    }

    onClickDetail(e) {
        this.setState({
            isRedirectToDetail: true
        })
    }

    render() {
        const {user} = this.state;
        const {isRedirectToDetail} = this.state;

        if (isRedirectToDetail) {
            return <Redirect to={"/user-detail/" + user.id}/>;
        }

        return (
            <tr key={user.id} onClick={this.onClickDetail}>
                <td>{user.identity.firstName}</td>
                <td>{user.identity.name}</td>
                <td>{user.username}</td>
                <td>{user.identity.telephone}</td>
            </tr>
        )
    }
}

export default UserTableLine;