import React from "react";
import Grid from "@material-ui/core/Grid";
import {Redirect} from "react-router";
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField/TextField";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Radio from "@material-ui/core/Radio/Radio";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import {Box} from "@material-ui/core";
import Button from "@material-ui/core/Button";

export default class AddNewUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: JSON.parse(localStorage.getItem('user')),
            addable: true,
            name: "",
            firstName: "",
            email: "",
            telephone: "",
            username: "",
            password: "",
            status: true,
            roles: [],
            isRedirectToUserList: false
        };

        this.handleValueName = this.handleValueName.bind(this);
        this.handleValueFirstName = this.handleValueFirstName.bind(this);
        this.handleValueEmail = this.handleValueEmail.bind(this);
        this.handleValueTelephone = this.handleValueTelephone.bind(this);
        this.handleValueUsername = this.handleValueUsername.bind(this);
        this.handleValuePassword = this.handleValuePassword.bind(this);
        this.handleValueStatus = this.handleValueStatus.bind(this);
        this.handleValueRoles = this.handleValueRoles.bind(this);
        this.isAddable = this.isAddable.bind(this);
        this.addNewUser = this.addNewUser.bind(this);
        this.redirectToUserList = this.redirectToUserList.bind(this);
    }

    redirectToUserList() {
        this.setState({
            isRedirectToUserList: true
        })
    }

    addNewUser() {
        fetch('http://localhost:8080/rest/users/newUser', {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + this.state.currentUser.token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                identity: {
                    name: this.state.name,
                    firstName: this.state.firstName,
                    email: this.state.email,
                    telephone: this.state.telephone
                },
                username: this.state.username,
                password: this.state.password,
                status: this.state.status,
                roles: this.state.roles
            })
        });
        this.redirectToUserList();
    }

    isAddable() {
        const {
            name,
            firstName,
            email,
            telephone,
            username,
            password,
            status,
            roles
        } = this.state;
        if (name !== "" &&
            firstName !== "" &&
            email !== "" &&
            telephone !== "" &&
            username !== "" &&
            password !== "" &&
            status !== null &&
            roles.length > 0) {
            this.setState({addable: false})
        } else {
            this.setState({addable: true})
        }
    }

    handleValueName(value) {
        this.setState({name: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValueFirstName(value) {
        this.setState({firstName: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValueEmail(value) {
        this.setState({email: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValueTelephone(value) {
        this.setState({telephone: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValueUsername(value) {
        this.setState({username: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValuePassword(value) {
        this.setState({password: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValueStatus(value) {
        this.setState({status: value.target.value},() =>
            this.isAddable()
        )
    }

    handleValueRoles(value) {
        this.setState({roles: value}, () =>
            this.isAddable()
        )
    }

    render() {

        const {
            isRedirectToUserList,
            name,
            firstName,
            email,
            telephone,
            username,
            password,
            status,
            roles
        } = this.state;


        const availableRoles = [
            { id: 2, name: 'ROLE_USER' },
            { id: 1, name: 'ROLE_ADMIN' }
        ];

        let tempRoles = [];
        for (let i = 0 ; i < roles.length ; i++) {
            tempRoles.push({id: roles[i].id, name: roles[i].name});
        }

        if (isRedirectToUserList) {
            return <Redirect to={"/user-list"}/>;
        }

        return (
            <Grid container justify={"center"}>
                <Grid item sm={8}>
                    <Card>
                        <CardContent>
                            <React.Fragment>
                                <Typography variant="h6" gutterBottom>
                                    Ajout d'un utilisateur
                                </Typography>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="FirstName"
                                            name="FirstName"
                                            label="Prénom"
                                            value={firstName}
                                            fullWidth
                                            onChange={this.handleValueFirstName}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Name"
                                            name="Name"
                                            label="Nom"
                                            value={name}
                                            fullWidth
                                            onChange={this.handleValueName}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Telephone"
                                            name="Telephone"
                                            label="Téléphone"
                                            value={telephone}
                                            fullWidth
                                            onChange={this.handleValueTelephone}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Email"
                                            name="Email"
                                            label="Email"
                                            value={email}
                                            fullWidth
                                            onChange={this.handleValueEmail}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Username"
                                            name="Username"
                                            label="Login"
                                            value={username}
                                            fullWidth
                                            onChange={this.handleValueUsername}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Password"
                                            name="Password"
                                            label="Mot de passe"
                                            type={"password"}
                                            value={password}
                                            fullWidth
                                            onChange={this.handleValuePassword}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Statut</FormLabel>
                                        <RadioGroup aria-label="status" value={status} name="status"
                                                    onChange={this.handleValueStatus}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="ACTIVÉ" checked={status} control={<Radio/>} label="Activé"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="DÉSACTIVÉ" checked={!status} control={<Radio/>}
                                                                      label="Désactivé"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Autocomplete
                                            multiple
                                            id="tags-standard"
                                            options={availableRoles}
                                            value={tempRoles}
                                            getOptionLabel={(option) => option.name === "ROLE_ADMIN" ? "Administrateur" : "Utilisateur"}
                                            getOptionSelected={(option, value) => value.id === option.id}
                                            onChange={(_, selectedOptions) => this.handleValueRoles(selectedOptions)}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="standard"
                                                    label="Rôles"
                                                />
                                            )}
                                        />
                                    </Grid>
                                    <Grid container justify={"center"} direction={"row"}>
                                        <Box sm={2}/>
                                        <Grid item sm={2}>
                                            <Button variant="contained" color="primary" onClick={this.addNewUser}
                                                    disabled={this.state.addable}>
                                                Ajouter
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </React.Fragment>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }
}