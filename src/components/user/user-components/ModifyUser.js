import React from "react";
import {Redirect} from "react-router";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card/Card";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField/TextField";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Radio from "@material-ui/core/Radio/Radio";
import {Box} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class ModifyUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.idUser,
            currentUser: JSON.parse(localStorage.getItem('user')),
            addable: true,
            name: "",
            firstName: "",
            email: "",
            telephone: "",
            username: "",
            status: true,
            roles: [],
            isRedirectToUserDetail: false,
            isRedirectToUserList: false
        };

        this.handleValueName = this.handleValueName.bind(this);
        this.handleValueFirstName = this.handleValueFirstName.bind(this);
        this.handleValueEmail = this.handleValueEmail.bind(this);
        this.handleValueTelephone = this.handleValueTelephone.bind(this);
        this.handleValueUsername = this.handleValueUsername.bind(this);
        this.handleValueStatus = this.handleValueStatus.bind(this);
        this.handleValueRoles = this.handleValueRoles.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.redirectToUserList = this.redirectToUserList.bind(this);
        this.onClickUserDetail = this.onClickUserDetail.bind(this);
        this.isAddable = this.isAddable.bind(this);
    }

    componentDidMount() {
        fetch("http://localhost:8080/rest/users/user/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.currentUser.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState( {
                idUser: data.id,
                name: data.identity.name,
                firstName: data.identity.firstName,
                email: data.identity.email,
                telephone: data.identity.telephone,
                username: data.username,
                password: data.password,
                status: data.status,
                roles: data.roles
            }, () => this.isAddable()));
    }

    redirectToUserList() {
        this.setState({
            isRedirectToUserList: true
        });
    }

    updateUser() {
        let jsonUser = JSON.stringify({
            id: this.state.idUser,
            identity: {
                name: this.state.name,
                firstName: this.state.firstName,
                telephone: this.state.telephone,
                email: this.state.email
            },
            username: this.state.username,
            password: this.state.password,
            status: this.state.status === 'ACTIVÉ',
            roles: this.state.roles
        });
        fetch('http://localhost:8080/rest/users/updateUser', {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + this.state.currentUser.token,
                'Content-Type': 'application/json'
            },
            body: jsonUser
        });
        this.redirectToUserList();
    }

    onClickUserDetail() {
        this.setState({
            isRedirectToUserDetail: true
        })
    }

    isAddable() {
        const {
            name,
            firstName,
            email,
            telephone,
            username,
            status,
            roles
        } = this.state;
        if (name !== ""
            && firstName !== ""
            && email !== ""
            && telephone !== ""
            && username !== ""
            && status !== null
            && roles.length > 0) {
            this.setState({addable: false})
        } else {
            this.setState({addable: true})
        }
    }

    handleValueName(value) {
        this.setState({name: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueFirstName(value) {
        this.setState({firstName: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueEmail(value) {
        this.setState({email: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueTelephone(value) {
        this.setState({telephone: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueUsername(value) {
        this.setState({username: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueStatus(value) {
        this.setState({status: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueRoles(value) {
        this.setState({roles: value}, () =>
            this.isAddable()
        )
    }


    render() {
        const {
            isRedirectToUserList,
            isRedirectToUserDetail,
            name,
            firstName,
            email,
            telephone,
            username,
            status,
            roles,
            id
        } = this.state;

        const availableRoles = [
            { id: 2, name: 'ROLE_USER' },
            { id: 1, name: 'ROLE_ADMIN' }
        ];

        let tempRoles = [];
        for (let i = 0 ; i < roles.length ; i++) {
            tempRoles.push({id: roles[i].id, name: roles[i].name});
        }

        if (isRedirectToUserList) {
            return <Redirect to={"/user-list"}/>;
        }

        if (isRedirectToUserDetail) {
            return <Redirect to={"/user-detail/" + id}/>;
        }

        return (
            <Grid container justify={"center"}>
                <Grid item sm={8}>
                    <Card>
                        <CardContent>
                            <React.Fragment>
                                <Typography variant="h6" gutterBottom>
                                    Modification d'un utilisateur
                                </Typography>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="FirstName"
                                            name="FirstName"
                                            label="Prénom"
                                            value={firstName}
                                            fullWidth
                                            onChange={this.handleValueFirstName}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Name"
                                            name="Name"
                                            label="Nom"
                                            value={name}
                                            fullWidth
                                            onChange={this.handleValueName}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Telephone"
                                            name="Telephone"
                                            label="Téléphone"
                                            value={telephone}
                                            fullWidth
                                            onChange={this.handleValueTelephone}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Email"
                                            name="Email"
                                            label="Email"
                                            value={email}
                                            fullWidth
                                            onChange={this.handleValueEmail}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Username"
                                            name="Username"
                                            label="Login"
                                            value={username}
                                            fullWidth
                                            onChange={this.handleValueUsername}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Statut</FormLabel>
                                        <RadioGroup aria-label="status" value={status} name="status"
                                                    onChange={this.handleValueStatus}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="ACTIVÉ" checked={status} control={<Radio/>} label="Activé"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="DÉSACTIVÉ" checked={!status} control={<Radio/>}
                                                                      label="Désactivé"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Autocomplete
                                            multiple
                                            id="tags-standard"
                                            options={availableRoles}
                                            value={tempRoles}
                                            getOptionLabel={(option) => option.name === "ROLE_ADMIN" ? "Administrateur" : "Utilisateur"}
                                            getOptionSelected={(option, value) => value.id === option.id}
                                            onChange={(_, selectedOptions) => this.handleValueRoles(selectedOptions)}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="standard"
                                                    label="Rôles"
                                                />
                                            )}
                                        />
                                    </Grid>
                                    <Grid container justify={"center"} direction={"row"}>
                                        <Box sm={2}/>
                                        <Grid item sm={2}>
                                            <Button variant="contained" color="primary" onClick={this.updateUser}
                                                    disabled={this.state.addable}>
                                                Modifier
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </React.Fragment>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }
}