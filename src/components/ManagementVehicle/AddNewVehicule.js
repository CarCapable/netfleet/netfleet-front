import React from "react";
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
import {Redirect} from "react-router";
import ImageUploading from "react-images-uploading/dist";
import Box from "@material-ui/core/Box";
import {getBackUrl} from "../../config/env";

export default class AddNewVehicule extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: JSON.parse(localStorage.getItem('user')),
            addable: true,
            brand: "",
            model: "",
            nbSeats: 0,
            trunkCapacity: 0,
            type: "",
            statut: "",
            vinCode: "",
            fuel: "",
            image: "",
            isRedirectToManagementVehicule: false,
            keysPosition: "",
            siteName: "",
            number: 0,
            street: "",
            city: "",
            postalCode: "",
            site: []
        };
        this.handleValueBrand = this.handleValueBrand.bind(this)
        this.handleValueModel = this.handleValueModel.bind(this)
        this.handleValueNbSeats = this.handleValueNbSeats.bind(this)
        this.handleValueTrunkCapacity = this.handleValueTrunkCapacity.bind(this)
        this.handleValueType = this.handleValueType.bind(this)
        this.handleValueStatut = this.handleValueStatut.bind(this)
        this.handleValueVinCode = this.handleValueVinCode.bind(this)
        this.handleValueFuel = this.handleValueFuel.bind(this)
        this.addNewVehicule = this.addNewVehicule.bind(this)
        this.redirectToManagement = this.redirectToManagement.bind(this)
        this.handleValueSiteName = this.handleValueSiteName.bind(this)
        this.handleValueNumber = this.handleValueNumber.bind(this)
        this.handleValueStreet = this.handleValueStreet.bind(this)
        this.handleValueCity = this.handleValueCity.bind(this)
        this.handleValuePostalCode = this.handleValuePostalCode.bind(this)
        this.handleValueKeysPosition = this.handleValueKeysPosition.bind(this)
        this.uploadImg = this.uploadImg.bind(this)
        this.selectSitesVehicle = this.selectSitesVehicle.bind(this)
    }

    componentDidMount() {
        fetch(getBackUrl() + "rest/sites/sans-vehicle",
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({site: data}))
    }

    redirectToManagement() {
        this.setState({
            isRedirectToManagementVehicule: true
        })
    }

    addNewVehicule() {
        fetch(getBackUrl() + "rest/vehicles/newVehicle", {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + this.state.user.token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                vinCode: this.state.vinCode,
                brand: this.state.brand,
                model: this.state.model,
                statut: this.state.statut,
                fuel: this.state.fuel,
                nbSeats: this.state.nbSeats,
                trunkCapacity: this.state.trunkCapacity,
                stickGear: this.state.stickGear === "AUTOMATIC",
                image: this.state.image,
                keysPosition: this.state.keysPosition,
                site: {
                    siteName: this.state.siteName,
                    number: this.state.number,
                    street: this.state.street,
                    city: this.state.city,
                    postalCode: this.state.postalCode
                }
            })
        })
        this.redirectToManagement();
    }


    isAddable() {
        const {
            brand,
            model,
            nbSeats,
            trunkCapacity,
            type,
            statut,
            vinCode,
            fuel,
            keysPosition,
            siteName,
            number,
            street,
            city,
            postalCode
        } = this.state;
        if (brand !== ""
            && model !== ""
            && nbSeats > 0
            && trunkCapacity > 0
            && type !== ""
            && statut !== "" && vinCode !== ""
            && fuel !== ""
            && keysPosition !== ""
            && siteName !== ""
            && number > 0
            && street !== ""
            && city !== ""
            && postalCode !== "") {
            this.setState({addable: false})
        } else {
            this.setState({addable: true})
        }
    }

    handleValueBrand(value) {
        this.setState({brand: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueModel(value) {
        this.setState({model: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueNbSeats(value) {
        this.setState({nbSeats: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueTrunkCapacity(value) {
        this.setState({trunkCapacity: value.target.value}, () =>
            this.isAddable()
        )
    };

    handleValueType(value) {
        this.setState({type: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueStatut(value) {
        this.setState({statut: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueVinCode(value) {
        this.setState({vinCode: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueFuel(value) {
        this.setState({fuel: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueKeysPosition(value) {
        this.setState({keysPosition: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueSiteName(value) {
        this.setState({siteName: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueNumber(value) {
        this.setState({number: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueStreet(value) {
        this.setState({street: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueCity(value) {
        this.setState({city: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValuePostalCode(value) {
        this.setState({postalCode: value.target.value}, () =>
            this.isAddable()
        )
    }

    uploadImg(imageList) {
        this.setState({image: imageList[0].dataURL})
    }

    selectSitesVehicle(site) {
        console.log(site)
    }

    render() {
        const {isRedirectToManagementVehicule} = this.state;

        if (isRedirectToManagementVehicule) {
            return <Redirect to={"/management-vehicle"}/>;
        }

        return (
            <Grid container justify={"center"}>
                <Grid item sm={8}>
                    <Card>
                        <CardContent>
                            <React.Fragment>
                                <Typography variant="h6" gutterBottom>
                                    Ajout d'un véhicule
                                </Typography>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Brand"
                                            name="Brand"
                                            label="Marque"
                                            fullWidth
                                            onChange={this.handleValueBrand}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Model"
                                            name="Model"
                                            label="Modele"
                                            fullWidth
                                            onChange={this.handleValueModel}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="nbSeats"
                                            name="nbSeats"
                                            type="number"
                                            label="Nombre de place"
                                            fullWidth
                                            onChange={this.handleValueNbSeats}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="trunkCapacity"
                                            name="trunkCapacity"
                                            type="number"
                                            label="Capacité du coffre"
                                            fullWidth
                                            onChange={this.handleValueTrunkCapacity}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Type de vehicule</FormLabel>
                                        <RadioGroup aria-label="type" name="type" onChange={this.handleValueType}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="MANUAL" control={<Radio/>} label="Manuel"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="AUTOMATIC" control={<Radio/>}
                                                                      label="Automatique"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Statut du véhicule</FormLabel>
                                        <RadioGroup aria-label="statut" name="statut1"
                                                    onChange={this.handleValueStatut}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="AVAILABLE" control={<Radio/>}
                                                                      label="Disponible"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="UNAVAILABLE" control={<Radio/>}
                                                                      label="Indisponible"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="BOOKED" control={<Radio/>}
                                                                      label="Réservé"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="vinCode"
                                            name="vinCode"
                                            label="Immatriculation"
                                            fullWidth
                                            onChange={this.handleValueVinCode}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Type d'essence</FormLabel>
                                        <RadioGroup aria-label="fuel" name="fuel" onChange={this.handleValueFuel}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="PETROL" control={<Radio/>}
                                                                      label="Essence"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="DIESEL" control={<Radio/>} label="Diesel"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="GPL" control={<Radio/>} label="GPL"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={12}>
                                        <TextField
                                            required
                                            id="keysPosition"
                                            name="keysPosition"
                                            label="Emplacement des clés"
                                            fullWidth
                                            onChange={this.handleValueKeysPosition}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={12}>
                                        <Typography variant="h6" gutterBottom>
                                            Ajout du site de rattachement
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="siteName"
                                            name="siteName"
                                            label="Nom du site"
                                            fullWidth
                                            onChange={this.handleValueSiteName}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="number"
                                            name="number"
                                            type="number"
                                            label="Numéro de rue"
                                            fullWidth
                                            onChange={this.handleValueNumber}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="street"
                                            name="street"
                                            label="Nom de la rue"
                                            fullWidth
                                            onChange={this.handleValueStreet}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="city"
                                            name="city"
                                            label="Nom de la ville"
                                            fullWidth
                                            onChange={this.handleValueCity}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={12}>
                                        <TextField
                                            required
                                            id="postalCode"
                                            name="postalCode"
                                            label="Code postal"
                                            fullWidth
                                            onChange={this.handleValuePostalCode}
                                        />
                                    </Grid>

                                    <Grid container justify={"center"}>
                                        <Grid item xs={2} sm={8}>
                                            <ImageUploading multiple onChange={this.uploadImg}
                                                            maxNumber={69}>
                                                {({imageList, onImageUpload}) => (
                                                    // write your building UI
                                                    <Grid container justify={"center"}>
                                                        <Button onClick={onImageUpload} variant="contained"
                                                                color="default">Upload images</Button>
                                                        {imageList.map(image => (
                                                            <Grid container justify={"center"} key={image.key}>
                                                                <img src={image.dataURL} alt="" width="250"/>
                                                                <Grid container justify={"center"} sm={12}>

                                                                    <Button onClick={image.onUpdate}
                                                                            variant="contained" color="default">Modifier
                                                                    </Button>
                                                                    <Box m={2}/>
                                                                    <Button onClick={image.onRemove}
                                                                            variant="contained" color="default">Supprimer
                                                                    </Button>

                                                                </Grid>
                                                            </Grid>
                                                        ))}
                                                    </Grid>
                                                )}
                                            </ImageUploading>
                                        </Grid>
                                    </Grid>
                                    <Box m={2}/>
                                    <Grid container justify={"center"}>
                                        <Grid item sm={2}>
                                            <Button variant="contained" color="primary" onClick={this.addNewVehicule}
                                                    disabled={this.state.addable}>
                                                Ajouter
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </React.Fragment>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }

}
