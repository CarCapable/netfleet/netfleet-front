import React from "react";
import Grid from '@material-ui/core/Grid';
import Paper from "@material-ui/core/Paper/Paper";
import {getBackUrl} from "../../config/env";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import {Redirect} from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import Divider from "@material-ui/core/Divider";
import DisplayReservation from "../reservation/DisplayReservation";

export default class DetailsVehicule extends React.Component {

    constructor(props) {
        super(props);
        let today = new Date();
        this.state = {
            id: props.match.params.idVehicule,
            user: JSON.parse(localStorage.getItem('user')),
            vehicule: {},
            vehicleActive: false,
            reservation: [],
            currentDate: today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            isRedirectToModifyVehicle: false,
            loading: false
        };
        this.onClickModifyVehicle = this.onClickModifyVehicle.bind(this);
    }

    componentDidMount() {
        fetch(getBackUrl() + "rest/vehicles/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({
                vehicule: {
                    id: data.id,
                    brand: data.brand,
                    fuel: data.fuel.value,
                    model: data.model,
                    nbSeats: data.nbSeats,
                    trunkCapacity: data.trunkCapacity,
                    stickGear: data.stickGear ? "MANUEL" : "AUTOMATIQUE",
                    image: data.image,
                    keysPosition: data.keysPosition,
                    vinCode: data.vinCode
                }
            }));

        fetch(getBackUrl() + "rest/reservationRequests/vehicle/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({
                reservation: data, loading: true
            }))
    }


    onClickModifyVehicle() {
        this.setState({
            isRedirectToModifyVehicle: true
        })
    }


    render() {

        if (!this.state.loading) {
            return <Grid container justify={"center"}>
                <CircularProgress color="secondary"/>
            </Grid>
        }

        const {
            vehicule, isRedirectToModifyVehicle, reservation
        } = this.state;

        if (isRedirectToModifyVehicle) {
            return <Redirect to={"/management-vehicle/modifier-vehicle/" + vehicule.id}/>;
        }

        return (
            <Grid container alignItems={"center"} direction={"column"} spacing={1}>
                <Box m={2}/>

                <Grid item sm={12}>
                    <Paper elevation={3}>
                        <img src={vehicule.image} width={750}/>
                    </Paper>
                </Grid>

                <Box m={2}/>

                <Grid item sm={12}>
                    <Box sm={5}/>
                    <Button variant="contained" color="primary" onClick={this.onClickModifyVehicle}>Modifier le
                        véhicule</Button>
                </Grid>

                <Box m={2}/>

                {/* Container of 2 row */}
                <Grid container justify={"center"} direction={"row"} spacing={2}>
                    {/* FIRST ROW */}
                    <Grid item sm={4} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Modèle du véhicule</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.model}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Marque</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.brand}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Nombre de place</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.nbSeats}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    {/* SECOND ROW */}
                    <Grid item sm={4} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Capacité du coffre</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.trunkCapacity} L</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Carburant</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.fuel}</p>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item sm={4} xs={10}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Boite de vitesse</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.stickGear}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    <Grid item sm={6} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Position des clés</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.keysPosition}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    <Grid item sm={6} xs={12}>
                        <Paper elevation={3}>
                            <Grid style={{textAlign: "center"}}>
                                <p className="h2">Immatriculation</p>
                                <Divider variant="middle"/>
                                <p className="h3">{vehicule.vinCode}</p>
                            </Grid>
                        </Paper>
                    </Grid>

                    <Grid container item sm={12} direction={"row"} justify={"center"}>
                        {this.state.reservation.map((reservation, index) => {
                            return <DisplayReservation key={index} reservation={reservation}/>
                        })}
                    </Grid>
                </Grid>

            </Grid>
        )
    }
}
