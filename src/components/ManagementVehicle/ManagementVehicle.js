import React                from "react";
import FilterSearchVehicule from './management-vehicule-component/FilterSearchVehicule'
import Button               from '@material-ui/core/Button';
import Grid                 from '@material-ui/core/Grid';
import DisplayVehicule      from "./management-vehicule-component/DisplayVehicle";
import { Redirect }         from 'react-router';
import CircularProgress from "@material-ui/core/CircularProgress";
import {getBackUrl} from "../../config/env";

export default class ManagementVehicle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            message: "",
            typeOfVehicule: "",
            nbPlace: 0,
            user: props.store.user,
            vehicules: [],
            vehiculesState: [],
            isRedirectToAddNewVehicule: false,
            loading: false
        }
        localStorage.setItem("user", JSON.stringify(this.state.user))
        this.onClickAddNewVehicule = this.onClickAddNewVehicule.bind(this)
    };

    onClickAddNewVehicule() {
        this.setState({
            isRedirectToAddNewVehicule: true
        })
    }

    onChangeTypeOfVehicule = (typeOfVehicule) => {
        if (typeOfVehicule !== this.state.typeOfVehicule) {
            this.setState({typeOfVehicule: typeOfVehicule}, () => {
                this.filterVehicule()
            })
        }
    }

    onChangeNbPlace = (nbPlace) => {
        if (nbPlace !== this.state.nbPlace) {
            this.setState({nbPlace: nbPlace}, () => {
                this.filterVehicule()
            })
        }
    }

    filterVehicule() {
        this.setState({vehicules: this.state.vehiculesState});

        let vehiculesFiltered = this.state.vehiculesState;

        if (this.state.nbPlace !== 0 && this.state.nbPlace !== "Selectionner le nombre de place") {
            vehiculesFiltered = vehiculesFiltered.filter(vehicule => vehicule.nbSeats === parseInt(this.state.nbPlace));
        }

        if (this.state.typeOfVehicule !== "" && this.state.typeOfVehicule !== "Selectionner le type") {
            vehiculesFiltered = vehiculesFiltered.filter(vehicule => vehicule.fuel.value === this.state.typeOfVehicule);
        }

        this.setState({vehicules: vehiculesFiltered})
    }

    componentDidMount() {
        fetch(getBackUrl()+"rest/vehicles",
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({vehicules: data, vehiculesState: data, loading: true}))
    }

    render() {

        if (this.state.isRedirectToAddNewVehicule) {
            return <Redirect to={"/management-vehicle/ajouter-vehicule"}/>;
        }

        if(!this.state.loading){
            return <Grid container justify={"center"}>
                <CircularProgress color="secondary" />
            </Grid>
        }

        return (
            <Grid container justify="center" alignItems={"center"} spacing={4}>
                <Grid item sm={8}>
                    <Button variant="contained" color="primary" onClick={this.onClickAddNewVehicule} fullWidth>Ajouter un vehicle</Button>
                </Grid>
                <Grid item sm={8}>
                    <FilterSearchVehicule typeOfVehiculeChange={this.onChangeTypeOfVehicule}
                                          nbPlaceChange={this.onChangeNbPlace}/>
                </Grid>
                <Grid container spacing={3} alignItems={"center"} justify="center">
                    <Grid item sm={8}>
                        {this.state.vehicules.map((vehicule, index) => {
                            return <DisplayVehicule key={index} vehicule={vehicule}/>
                        })}
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}
