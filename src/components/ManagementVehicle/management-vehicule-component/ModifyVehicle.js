import React from "react";
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import FormLabel from "@material-ui/core/FormLabel";
import Button from "@material-ui/core/Button";
import {Redirect} from "react-router";
import ImageUploading from "react-images-uploading/dist";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import {Box} from "@material-ui/core";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {getBackUrl} from "../../../config/env";

export default class ModifyVehicle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.idVehicule,
            user: JSON.parse(localStorage.getItem('user')),
            addable: true,
            brand: "",
            model: "",
            nbSeats: 0,
            trunkCapacity: 0,
            statut: "",
            vinCode: "",
            stickGear: "",
            fuel: "",
            idVehicule: 0,
            image: "",
            isRedirectToManagementVehicule: false,
            keysPosition: "",
            site: [],
            anchorE1: null,
            index: 1,
            idSite: 0,
            firstimeSelectedList: true,
            isRedirectToDetailsVehicle: false
        };
        this.handleValueBrand = this.handleValueBrand.bind(this)
        this.handleValueModel = this.handleValueModel.bind(this)
        this.handleValueNbSeats = this.handleValueNbSeats.bind(this)
        this.handleValueTrunkCapacity = this.handleValueTrunkCapacity.bind(this)
        this.handleValueType = this.handleValueType.bind(this)
        this.handleValueStatut = this.handleValueStatut.bind(this)
        this.handleValueVinCode = this.handleValueVinCode.bind(this)
        this.handleValueFuel = this.handleValueFuel.bind(this)
        this.updateVehicule = this.updateVehicule.bind(this)
        this.redirectToManagement = this.redirectToManagement.bind(this)
        this.handleValueKeysPosition = this.handleValueKeysPosition.bind(this)
        this.uploadImg = this.uploadImg.bind(this)
        this.onClickDetailsVehicule = this.onClickDetailsVehicule.bind(this)
    }

    componentDidMount() {
        fetch(getBackUrl() + "rest/vehicles/" + this.state.id,
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({
                idVehicule: data.id,
                brand: data.brand,
                fuel: data.fuel.value,
                model: data.model,
                nbSeats: data.nbSeats,
                trunkCapacity: data.trunkCapacity,
                stickGear: data.stickGear ? "MANUEL" : "AUTOMATIQUE",
                image: data.image,
                statut: data.statut.value,
                vinCode: data.vinCode,
                keysPosition: data.keysPosition,
                idSite: data.site.id
            }, () => this.isAddable()));

        fetch(getBackUrl() + "rest/sites/sans-vehicle",
            {
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "GET"
            }
        )
            .then(response => response.json())
            .then(data => this.setState({site: data}))
    }

    redirectToManagement() {
        this.setState({
            isRedirectToDetailsVehicle: true
        })
    }

    updateVehicule() {
        fetch(getBackUrl() + "rest/vehicles/updateVehicle", {
            method: 'POST',
            headers: {
                'authorization': 'Bearer ' + this.state.user.token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: this.state.idVehicule,
                vinCode: this.state.vinCode,
                brand: this.state.brand,
                model: this.state.model,
                statut: this.state.statut,
                fuel: this.state.fuel,
                nbSeats: this.state.nbSeats,
                trunkCapacity: this.state.trunkCapacity,
                stickGear: this.state.stickGear === "MANUEL",
                image: this.state.image,
                keysPosition: this.state.keysPosition,
                site: this.state.site[this.state.index]
            })
        })
        this.redirectToManagement();
    }

    onClickDetailsVehicule() {
        this.setState({
            isRedirectToDetailsVehicle: true
        })
    }


    isAddable() {
        const {
            brand,
            model,
            nbSeats,
            trunkCapacity,
            stickGear,
            statut,
            vinCode,
            fuel,
            keysPosition,
        } = this.state;
        if (brand !== ""
            && model !== ""
            && nbSeats > 0
            && trunkCapacity > 0
            && stickGear !== ""
            && statut !== "" && vinCode !== ""
            && fuel !== ""
            && keysPosition !== "") {
            this.setState({addable: false})
        } else {
            this.setState({addable: true})
        }
    }

    handleValueBrand(value) {
        this.setState({brand: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueModel(value) {
        this.setState({model: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueNbSeats(value) {
        this.setState({nbSeats: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueTrunkCapacity(value) {
        this.setState({trunkCapacity: value.target.value}, () =>
            this.isAddable()
        )
    };

    handleValueType(value) {
        this.setState({stickGear: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueStatut(value) {
        this.setState({statut: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueVinCode(value) {
        this.setState({vinCode: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueFuel(value) {
        this.setState({fuel: value.target.value}, () =>
            this.isAddable()
        )
    }

    handleValueKeysPosition(value) {
        this.setState({keysPosition: value.target.value}, () =>
            this.isAddable()
        )
    }

    uploadImg(imageList) {
        this.setState({image: imageList[0].dataURL})
    }


    render() {
        const {
            isRedirectToManagementVehicule,
            brand,
            fuel,
            model,
            nbSeats,
            trunkCapacity,
            stickGear,
            image,
            statut,
            vinCode,
            keysPosition,
            anchorEl,
            index,
            site,
            idSite,
            firstimeSelectedList,
            isRedirectToDetailsVehicle,
            id
        } = this.state;

        if (isRedirectToManagementVehicule) {
            return <Redirect to={"/management-vehicle"}/>;
        }

        if (isRedirectToDetailsVehicle) {
            return <Redirect to={"/management-vehicle/details-vehicule/" + id}/>;
        }

        const handleClickListItem = (event) => {
            this.setState({anchorEl: event.currentTarget})
        };

        const handleMenuItemClick = (event, index) => {
            this.setState({index: index});
            this.setState({anchorEl: null})
        };

        const handleClose = () => {
            this.setState({anchorEl: null})
        };

        if (firstimeSelectedList) {
            site.forEach((site, index) => {
                if (site.id === idSite) {
                    this.setState({index: index, firstimeSelectedList: false})
                }
            })
        }
        return (
            <Grid container justify={"center"}>
                <Grid item sm={8}>
                    <Card>
                        <CardContent>
                            <React.Fragment>
                                <Typography variant="h6" gutterBottom>
                                    Modification d'un véhicule
                                </Typography>
                                <Grid container spacing={3}>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Brand"
                                            name="Brand"
                                            label="Marque"
                                            value={brand}
                                            fullWidth
                                            onChange={this.handleValueBrand}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="Model"
                                            name="Model"
                                            label="Modele"
                                            value={model}
                                            fullWidth
                                            onChange={this.handleValueModel}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="nbSeats"
                                            name="nbSeats"
                                            type="number"
                                            label="Nombre de place"
                                            value={nbSeats}
                                            fullWidth
                                            onChange={this.handleValueNbSeats}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="trunkCapacity"
                                            name="trunkCapacity"
                                            label="Capacité du coffre"
                                            value={trunkCapacity}
                                            fullWidth
                                            onChange={this.handleValueTrunkCapacity}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Type de vehicule</FormLabel>
                                        <RadioGroup aria-label="type" value={stickGear} name="type"
                                                    onChange={this.handleValueType}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="MANUEL" control={<Radio/>} label="Manuel"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="AUTOMATIQUE" control={<Radio/>}
                                                                      label="Automatique"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Statut du véhicule</FormLabel>
                                        <RadioGroup aria-label="statut" value={statut} name="statut"
                                                    onChange={this.handleValueStatut}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="AVAILABLE" control={<Radio/>}
                                                                      label="Disponible"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="UNAVAILABLE" control={<Radio/>}
                                                                      label="Indisponible"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="BOOKED" control={<Radio/>}
                                                                      label="Réservé"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            id="vinCode"
                                            name="vinCode"
                                            label="Immatriculation"
                                            value={vinCode}
                                            fullWidth
                                            onChange={this.handleValueVinCode}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormLabel component="legend" required={true}>Type d'essence</FormLabel>
                                        <RadioGroup aria-label="fuel" name="fuel" value={fuel}
                                                    onChange={this.handleValueFuel}>
                                            <Grid container>
                                                <Grid>
                                                    <FormControlLabel value="PETROL" control={<Radio/>}
                                                                      label="Essence"/>
                                                </Grid>
                                                <Grid>
                                                    <FormControlLabel value="DIESEL" control={<Radio/>} label="Diesel"/>
                                                </Grid>
                                            </Grid>
                                        </RadioGroup>
                                    </Grid>
                                    <Grid item xs={12} sm={12}>
                                        <TextField
                                            required
                                            id="keysPosition"
                                            name="keysPosition"
                                            label="Emplacement des clés"
                                            value={keysPosition}
                                            fullWidth
                                            onChange={this.handleValueKeysPosition}
                                        />
                                    </Grid>
                                    <Paper style={{maxHeight: 100, overflow: 'auto', width: '100%'}}>

                                        <Grid>
                                            <List component="nav" aria-label="Device settings">
                                                <ListItem
                                                    button
                                                    aria-haspopup="true"
                                                    aria-controls="lock-menu"
                                                    aria-label="when device is locked"
                                                    onClick={handleClickListItem}
                                                >
                                                    <ListItemText primary="Sélection du site de rattachement"
                                                                  secondary={site[index] !== undefined ? site[index].siteName : ""}/>
                                                </ListItem>
                                            </List>
                                            <Menu
                                                id="lock-menu"
                                                anchorEl={anchorEl}
                                                keepMounted
                                                open={Boolean(anchorEl)}
                                                onClose={handleClose}
                                            >
                                                {site.map((site, index) => (
                                                    <MenuItem
                                                        key={index}
                                                        disabled={index !== index}
                                                        selected={index === index}
                                                        onClick={(event) => handleMenuItemClick(event, index)}
                                                    >
                                                        {site.siteName}
                                                    </MenuItem>
                                                ))}
                                            </Menu>
                                        </Grid>
                                    </Paper>

                                    <Grid item sm={12}>
                                        <Paper elevation={3}>
                                            <img src={image} width={'100%'}/>
                                        </Paper>
                                    </Grid>

                                    <ImageUploading multiple onChange={this.uploadImg}
                                                    maxNumber={69}>
                                        {({imageList, onImageUpload}) => (
                                            // write your building UI
                                            <Grid container justify={"center"}>
                                                <Button onClick={onImageUpload} variant="contained"
                                                        color="default">Selectionner une image</Button>
                                                <Grid container justify={"center"}>
                                                    <img src={imageList !== undefined ? "" : imageList[0].dataURL}
                                                         alt="" width="250"/>
                                                </Grid>
                                            </Grid>
                                        )}
                                    </ImageUploading>

                                    <Grid container justify={"center"} direction={"row"}>
                                        <Box sm={2}/>
                                        <Grid item sm={2}>
                                            <Button variant="contained" color="primary" onClick={this.updateVehicule}
                                                    disabled={this.state.addable}>
                                                Modifier
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </React.Fragment>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }

}
