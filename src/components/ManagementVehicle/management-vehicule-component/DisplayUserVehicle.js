import React from "react";
import Grid from '@material-ui/core/Grid';
export default class DisplayUserVehicle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }


    render() {
        const onChangeTypeOfVehicule = (event) => {
            this.props.typeOfVehiculeChange (event.target.value);
        };

        const onChangeNbPlace = (event) => {
            this.props.nbPlaceChange (event.target.value);
        };

        return (
            <Grid container justify="center">
                <Grid item xs={6}>
                    <select className="browser-default custom-select" onChange={onChangeTypeOfVehicule}>
                        <option>Selectionner le type</option>
                        <option value="FUEL">FUEL</option>
                        <option value="DIESEL">DIESEL</option>
                    </select>
                </Grid>
                <Grid item xs={6}>
                    <select className="browser-default custom-select" onChange={onChangeNbPlace}>
                        <option>Selectionner le nombre de place</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </Grid>
            </Grid>
        )
    }

}