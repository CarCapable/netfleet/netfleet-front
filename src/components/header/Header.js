import * as React from "react";
import {Redirect} from "react-router";

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };
        this.OnClickHome = this.OnClickHome.bind(this);
    }

    OnClickHome() {

        this.setState({
            redirect: true
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        const {redirect} = this.state;
        if (redirect) {

            this.setState({
                redirect: false
            })
        }
    }

    render() {

        const {redirect} = this.state;

        if (redirect) {
            return (<Redirect to="/"/>)
        }
        return (
            <div className="hearder-1">
                <img id="logo" src={process.env.PUBLIC_URL + '/logo/logo_white_netfleet_240x249.png'} alt="logo"/>
                <div onClick={this.OnClickHome} className="title">NETFLEET</div>
            </div>
        )
    }
}

export default Header;