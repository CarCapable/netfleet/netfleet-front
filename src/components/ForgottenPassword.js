import * as React from "react";
import {Redirect} from "react-router";

class ForgottenPassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            errorEmail: false,
            redirect: false,
            isSend: false,
        };

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onClickSend = this.onClickSend.bind(this);
        this.onClickBack = this.onClickBack.bind(this);
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value,
            errorEmail: false
        })
    }

    onClickSend() {
        const {email} = this.state;

        this.setState({
            errorEmail: email === "",
            isSend: email !== "",
        });
    }

    onClickBack() {

        this.setState({
            redirect: true,
        });
    }

    render() {

        const {errorEmail, redirect, isSend} = this.state;

        if (redirect) {
            return <Redirect to='/login'/>;
        }

        return (
            <div id="login">
                <div className="middle-box">
                    <div>
                        <input onChange={this.onChangeEmail} type="text" placeholder="Email"/>
                    </div>
                    <div className="escape text-error">
                        {errorEmail &&
                        <>Erreur : Adresse mail invalide !</>
                        }
                    </div>
                    <hr/>
                    <button onClick={this.onClickBack}>Retour</button>
                    <button onClick={this.onClickSend}>Envoyer</button>
                </div>
                {isSend &&
                    <div className="box-fixed-bottom">
                        Email envoyé !
                    </div>
                }
            </div>
        )
    }
}

export default ForgottenPassword;