import Home from "../components/Home";
import {connect} from "react-redux";
import {removeCurrentUser} from "../actions/UserActions";

const mapStateToProps = store => {
    return {
        store: {
            user: store.user
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logout: () => {

            dispatch(removeCurrentUser());
        }
    };
};

const HomeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

export default HomeContainer;