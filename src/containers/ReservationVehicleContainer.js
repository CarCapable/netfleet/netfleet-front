import {connect} from "react-redux";
import Reservation from "../components/reservation/Reservation";

const mapStateToProps = store => {
    return {
        store: {
            user: store.user
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const ReservationVehicleContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Reservation);

export default ReservationVehicleContainer;