import Login from "../components/Login";
import {connect} from "react-redux";
import {connexion, anthentificationInitialError} from "../actions/AuthenActions";

const mapStateToProps = store => {
    return {
        store: {
            error: store.user.error
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {
        connexion: (email, password) => {
            dispatch(connexion(email, password));
        },
        initialError: () => {
            dispatch(anthentificationInitialError());
        }
    };
};

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);

export default LoginContainer;