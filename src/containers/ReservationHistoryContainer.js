import ReservationHistory from "../components/ReservationHistory/ReservationHistory";
import {connect} from "react-redux";
import {getHistoryReservationsByApplicantId} from "../actions/HistoryReservationActions";

const mapStateToProps = store => {
    return {
        store: {
            user: store.user,
            reservationsHistory : store.reservationsHistory
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getHistoryReservationByApplicantId: (applicantId) => {
            dispatch(getHistoryReservationsByApplicantId(applicantId));
        }
    };
};

const ReservationHistoryContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ReservationHistory);

export default ReservationHistoryContainer;