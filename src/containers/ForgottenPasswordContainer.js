import {connect} from "react-redux";
import ForgottenPassword from "../components/ForgottenPassword";

const mapStateToProps = store => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {};
};

const ForgottenPasswordContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ForgottenPassword);

export default ForgottenPasswordContainer;