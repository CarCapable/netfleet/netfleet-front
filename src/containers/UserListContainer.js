import UserList from "../components/user/UserList";
import {connect} from "react-redux";
import {getAllUsers, getUsersByRole, removeCurrentUser} from "../actions/UserActions";

const mapStateToProps = store => {
    return {
        store: {
            currentUser: store.user.currentUser,
            users: store.user.users
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getAllUsers: () => {

            dispatch(getAllUsers());
        },
        getUsersByRole: (role) => {
            dispatch(getUsersByRole(role));
        }
    };
};

const UserListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList);

export default UserListContainer;