import ManagementVehicule from "../components/ManagementVehicle/ManagementVehicle";
import {connect} from "react-redux";

const mapStateToProps = store => {
    return {
        store: {
            user: store.user
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

const VehiculeManagementContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ManagementVehicule);

export default VehiculeManagementContainer;