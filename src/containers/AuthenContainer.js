import {connect} from "react-redux";
import Authen from "../components/Authen";
import {removeCurrentUser} from "../actions/UserActions";

const mapStateToProps = store => {
    return {
        store: {
            user: store.user
        }
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logout: () => {

            dispatch(removeCurrentUser());
        }};
};

const AuthenContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Authen);

export default AuthenContainer;